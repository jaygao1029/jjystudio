//
//  AppDelegate.h
//  LFNostop
//
//  Created by gao jay on 3/27/14.
//  Copyright (c) 2014 lefun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

//
//  ViewController.h
//  LFNostop
//
//  Created by gao jay on 3/27/14.
//  Copyright (c) 2014 lefun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdMoGoDelegateProtocol.h"
#import "AdMoGoView.h"
#import "AdMoGoWebBrowserControllerUserDelegate.h"

@interface ViewController : UIViewController<AdMoGoDelegate,AdMoGoWebBrowserControllerUserDelegate,UIWebViewDelegate>
{
    UIWebView *_webView;
    NSString *_score;
    NSString *_bestScore;
}

@property (nonatomic, retain) AdMoGoView *adView;
@end

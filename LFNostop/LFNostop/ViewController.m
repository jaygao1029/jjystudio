//
//  ViewController.m
//  LFNostop
//
//  Created by gao jay on 3/27/14.
//  Copyright (c) 2014 lefun. All rights reserved.
//

#import "ViewController.h"
#import "LFAdManager.h"
#import "ShareSDK/ShareSDK.h"
#import "MobClick.h"

#define kNoStop @"nostop"
#define kShareUrl @"http://mylady.duapp.com/2048.html"
#define kShareContent @"我在2048中获得了%@分，%@方块达成，快来下载玩玩，看看能不能超越我~ 下载地址：%@"
#define kShareTimeline @"我完成了%@块，快来下载玩玩，看看能不能超过我~"
#define kDefaultShareContent @"快来下载2048不能停吧"
#define kShareTitle @"2048不能停"
#define kLFNoStop_ShareUrl @"LFNoStop_ShareUrl"
#define kShareClickEvent @"ShareClick"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    NSString *htmlFile = [[NSBundle mainBundle] pathForResource:@"index" ofType:@"html" inDirectory:@"www"];
    
    for (id subview in _webView.subviews)
        if ([[subview class] isSubclassOfClass: [UIScrollView class]])
            ((UIScrollView *)subview).bounces = NO;
    
    NSString *root = [NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath],@"www"];
    NSData *htmlData = [NSData dataWithContentsOfFile:htmlFile];
    NSURL* url = [NSURL fileURLWithPath:root];
    [_webView loadData:htmlData MIMEType:@"text/html" textEncodingName:@"UTF-8" baseURL:url];
    [self.view addSubview:_webView];
    _webView.dataDetectorTypes = UIDataDetectorTypeNone;
    _webView.delegate = self;
    
    BOOL st = [[LFAdManager sharedManager] allowAdDisplayWithUMengConfig];
    BOOL reviewing = [[LFAdManager sharedManager] isReviewing];
    NSLog(@"allow ad: %d", st);
    NSLog(@"in review: %d", reviewing);
    CGSize fSize = [UIScreen mainScreen].bounds.size;
    if (st && !reviewing)
    {
        _adView = [[AdMoGoView alloc] initWithAppKey:MoGo_ID_IPhone
                                             adType:AdViewTypeNormalBanner                                adMoGoViewDelegate:self];
        _adView.clipsToBounds = YES;
        _adView.adWebBrowswerDelegate = self;
        _adView.frame = CGRectMake(0.0, fSize.height - 50, 320.0, 50.0);
        [self.view addSubview:_adView];
//        _adView.alpha = 0;
    }
}

- (UIImage *)getScreenshotForView:(UIView *)view
{
    // Create a graphics context with the target size
    // On iOS 4 and later, use UIGraphicsBeginImageContextWithOptions to take the scale into consideration
    // On iOS prior to 4, fall back to use UIGraphicsBeginImageContext
    CGSize imageSize = view.bounds.size;
    if (NULL != UIGraphicsBeginImageContextWithOptions)
        UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0);
    else
        UIGraphicsBeginImageContext(imageSize);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // -renderInContext: renders in the coordinate space of the layer,
    // so we must first apply the layer's geometry to the graphics context
    CGContextSaveGState(context);
    // Center the context around the window's anchor point
    CGContextTranslateCTM(context, [view center].x, [view center].y);
    // Apply the window's transform about the anchor point
    CGContextConcatCTM(context, [view transform]);
    // Offset by the portion of the bounds left of and above the anchor point
    CGContextTranslateCTM(context,
                          -[view bounds].size.width * [[view layer] anchorPoint].x,
                          -[view bounds].size.height * [[view layer] anchorPoint].y);
    
    // Render the layer hierarchy to the current context
    [[view layer] renderInContext:context];
    
    // Retrieve the screenshot image
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}

- (void)share
{
    [MobClick event:kShareClickEvent label:@"share"];
    UIImage *scoreImage = [UIImage imageNamed:@"Icon.png"];
    id<ISSCAttachment> attachImage = [ShareSDK pngImageWithImage:scoreImage];
    //构造分享内容
    NSString *shareUrl = kShareUrl;
    if ([MobClick getConfigParams:kLFNoStop_ShareUrl]) {
        shareUrl = [MobClick getConfigParams:kLFNoStop_ShareUrl];
    }
    NSString *shareContent = [NSString stringWithFormat:kShareContent,_score,_bestScore,shareUrl];
    NSString *timeLineTitle = [NSString stringWithFormat:kShareTimeline,_bestScore];
    id<ISSContent> publishContent = [ShareSDK content:shareContent
                                       defaultContent:kDefaultShareContent
                                                image:attachImage
                                                title:shareContent
                                                  url:shareUrl
                                          description:kDefaultShareContent
                                            mediaType:SSPublishContentMediaTypeText];
    
    //定制微信好友信息
    [publishContent addWeixinSessionUnitWithType:[NSNumber numberWithInteger:SSPublishContentMediaTypeNews]
                                         content:INHERIT_VALUE
                                           title:kShareTitle
                                             url:INHERIT_VALUE
                                      thumbImage:attachImage
                                           image:INHERIT_VALUE
                                    musicFileUrl:nil
                                         extInfo:nil
                                        fileData:nil
                                    emoticonData:nil];
    
    //定制微信朋友圈信息
    [publishContent addWeixinTimelineUnitWithType:[NSNumber numberWithInteger:SSPublishContentMediaTypeNews]
                                          content:INHERIT_VALUE
                                            title:timeLineTitle
                                              url:INHERIT_VALUE
                                       thumbImage:attachImage
                                            image:INHERIT_VALUE
                                     musicFileUrl:nil
                                          extInfo:nil
                                         fileData:nil
                                     emoticonData:nil];
    
    [ShareSDK showShareActionSheet:nil
                         shareList:nil
                           content:publishContent
                     statusBarTips:YES
                       authOptions:nil
                      shareOptions: nil
                            result:^(ShareType type, SSResponseState state, id<ISSPlatformShareInfo> statusInfo, id<ICMErrorInfo> error, BOOL end) {
                                if (state == SSResponseStateSuccess)
                                {
                                    NSLog(@"分享成功");
                                }
                                else if (state == SSResponseStateFail)
                                {
                                    NSLog(@"分享失败");
                                }
                            }];
}

- (BOOL)webView:(UIWebView *)webView
shouldStartLoadWithRequest:(NSURLRequest *)request
 navigationType:(UIWebViewNavigationType)navigationType
{
    NSURL *url = request.URL;
    if ([[url scheme] isEqualToString:kNoStop] == YES) {
        NSString *cmdString = [url resourceSpecifier];
        NSArray *cmds = [cmdString componentsSeparatedByString:@"?"];
        NSString *cmdInfo = cmds[0];
        if ([cmdInfo isEqualToString:@"//share"]) {
            NSArray *parameters = [cmds[1] componentsSeparatedByString:@"&"];
            for (NSString *paramter in parameters){
                NSArray *paraInfos = [paramter componentsSeparatedByString:@"="];
                if ([paraInfos[0] isEqualToString:@"score"]){
                    _score = paraInfos[1];
                }
                if ([paraInfos[0] isEqualToString:@"bestValue"]) {
                    _bestScore = paraInfos[1];
                }
            }
            [self share];
        }
    }
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

/*
 返回广告rootViewController old code
 */
- (UIViewController *)viewControllerForPresentingModalView
{
    return self;
}

/*
 浏览器将要展示
 */
- (void)webBrowserWillAppear
{
    
}

/*
 浏览器已经展示
 */
- (void)webBrowserDidAppear
{
    
}

/*
 浏览器将要关闭
 */
- (void)webBrowserWillClosed
{
    
}

/*
 浏览器已经关闭
 */
- (void)webBrowserDidClosed
{
    
}

/*
 浏览器分享
 url 浏览器打开url
 */
- (void)webBrowserShare:(NSString *)url
{
    
}

@end
